(function ( $ ) {
    $.fn.pageWalk = function(options) {

        // Set default options
        var settings = $.extend({
            parent: 'body'
        }, options );

        // Append progress bar to parent and set variables
        var b = $('<div/>', {class: 'page-progress'}).appendTo(settings.parent),
            c = window,
            d = document,
            e = 0,
            f = 0;

        var t, l = (new Date()).getTime();

        // Catch scroll
        $(d).scroll(function() {
            e = Math.round(100 * $(c).scrollTop() / ($(d).height() - $(c).height())), e > 1 ? (b.css({
                opacity: 100
            }).css("width", Math.round(e) + "%"), $(c).scrollTop() > f && b.removeClass("reverse"), $(c).scrollTop() < f && b.addClass("reverse")) : (b.css({
                opacity: 0
            })), f = $(c).scrollTop();

            var now = (new Date()).getTime();

            if(now - l > 300){
                b.addClass("walk");
                l = now;
            }

            clearTimeout(t);
            t = setTimeout(function(){
                b.removeClass("walk");
            }, 600);
        }), $(d).trigger("scroll");

    };
}( jQuery ));